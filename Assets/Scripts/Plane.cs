﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : Singleton<Plane>
{
    private Collider _collider;
    private Vector3 _lastMousePoint;

    public Vector3 lastMousePoint
    {
        get
        {
            return _lastMousePoint;
        }
        private set { }
    }

    private void Awake()
    {
        _collider = GetComponent<Collider>();
    }

    private void Update()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] raycastHits = Physics.RaycastAll(ray, 100f);
        
        foreach(var hit in raycastHits)
        if (hit.collider.Equals(_collider))
        {
            _lastMousePoint = hit.point;
        }
    }

    public Vector3 GetMouseAsPlanePoint()
    {
        return _lastMousePoint;
    }
}
