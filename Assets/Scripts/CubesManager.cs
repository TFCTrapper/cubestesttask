﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubesManager : Singleton<CubesManager>
{
    [SerializeField]
    private GameObject _cubePrefab;
    [SerializeField]
    private float _cubeSize;

    [Header("Level params")]
    [SerializeField]
    private int _levelLength;
    [SerializeField]
    private int _levelWidth;
    [SerializeField]
    private int _levelMinHeight;
    [SerializeField]
    private int _levelMaxHeight;

    private Transform _transform;

    public void Initialize()
    {
        _transform = GetComponent<Transform>();
    }

    public void GenerateLevel()
    {
        for (int x = 0; x < _levelLength; x++)
        {
            for (int z = 0; z < _levelWidth; z++)
            {
                int height = Random.Range(_levelMinHeight, _levelMaxHeight + 1);
                for (int y = 0; y < height; y++)
                {
                    Vector3 position = new Vector3(
                        _cubeSize * (-_levelLength / 2 + x),
                        _cubeSize * (0.5f + y),
                        _cubeSize * (-_levelWidth / 2 + z));
                    Instantiate(_cubePrefab, position, Quaternion.identity, _transform);
                }
            }
        }
    }
}
