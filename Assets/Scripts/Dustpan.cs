﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Dustpan : MonoBehaviour
{
    [SerializeField]
    private float _maxSpeed;
    [SerializeField]
    private float _minOffsetSqrForMaxSpeed;
    [SerializeField]
    private float _rotationDuration;

    private Vector3 _offset;
    private Rigidbody _rigidbody;
    private Transform _transform;
    private float _offsetSqrMagnitude;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _transform = GetComponent<Transform>();
    }

    void OnMouseDown()
    {
        _transform.DOLookAt(GetPointToLookAt(), _rotationDuration);
        _offset = gameObject.transform.position - Plane.Instance.lastMousePoint;
        _offsetSqrMagnitude = Mathf.Clamp(_offset.sqrMagnitude, 0, _minOffsetSqrForMaxSpeed);
    }

    private void OnMouseUp()
    {
        _rigidbody.velocity = Vector3.zero;
    }

    private void OnMouseDrag()
    {
        _transform.DOLookAt(GetPointToLookAt(), _rotationDuration);
        _offset = Plane.Instance.lastMousePoint - gameObject.transform.position;
        _offsetSqrMagnitude = Mathf.Clamp(_offset.sqrMagnitude, 0, _minOffsetSqrForMaxSpeed);
        _rigidbody.velocity = _offset.normalized * _maxSpeed * (_offsetSqrMagnitude / _minOffsetSqrForMaxSpeed);
    }

    private Vector3 GetPointToLookAt()
    {
        return new Vector3(Plane.Instance.lastMousePoint.x, transform.position.y, Plane.Instance.lastMousePoint.z);
    }
}
