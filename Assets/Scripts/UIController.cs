﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIController : Singleton<UIController>
{
    [SerializeField]
    private Slider _trashCansSlider;
    [SerializeField]
    private float _sliderChangeValueDuration;

    public void UpdateTrashCansSlider()
    {
        _trashCansSlider.DOValue(Game.Instance.GetComparedScore(), _sliderChangeValueDuration);
    }

    public void OnQuitClick()
    {
        Game.Instance.Quit();
    }

    public void OnRestartClick()
    {
        Game.Instance.Restart();
    }
}
