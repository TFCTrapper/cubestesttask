﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : Singleton<Game>
{
    [SerializeField]
    private TrashCanTrigger[] _trashCans = new TrashCanTrigger[2];

    public void Awake()
    {
        CubesManager.Instance.Initialize();
        CubesManager.Instance.GenerateLevel();
    }

    public float GetComparedScore()
    {
        return (float)_trashCans[0].score / (float)(_trashCans[0].score + _trashCans[1].score);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
