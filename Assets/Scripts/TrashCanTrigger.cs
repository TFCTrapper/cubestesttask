﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TrashCanSide
{
    Left = 0,
    Right
}

public class TrashCanTrigger : MonoBehaviour
{
    public int score { get; set; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cube"))
        {
            score++;
            other.tag = "Untagged";
            UIController.Instance.UpdateTrashCansSlider();
        }
    }
}
